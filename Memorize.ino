// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
#include "InternetButton.h"
#include "math.h"

InternetButton b = InternetButton();



String checkSequence = "";
String result = "";


void setup() {
 b.begin();
 
 initialSequence();  // this is requirement 5
 b.allLedsOff(); // So that the quiz sequence can begin
 delay(2000);
 
 initSequence("500"); // this is quiz sequence
 
 
Particle.function("playAgain", playAgain);
Particle.function("changeLevel",initSequence);
 
 
}

void loop() {
  
        userEnterSequence();
        checkResult();
        restartByPhoton();
      
        
    
   
}

void initialSequence() {
   
      for(int i = 1; i <= 3; i++) {
       
        b.allLedsOn( 0, 250, 4);
        delay(200);
        b.allLedsOff();
    }
     b.playSong("E5,8,G5,8,E6,8,C6,8,D6,8");
     
     b.allLedsOff();
     delay(1000);
     b.ledOn(1,0,0,255);
     b.ledOn(10,0,0,255);
     delay(1000);
    
    
       
   }


int initSequence(String delayLevel) {
   
    int delayValue = atoi(delayLevel.c_str());
   if (delayValue >= 250){
       
      b.ledOn(12, 255, 0, 0); // Red
    delay(delayValue);
    b.allLedsOff();
    
    b.ledOn(6, 0, 0, 255); // Blue
       delay(delayValue);
    b.allLedsOff();
    
    b.ledOn(3, 0, 255, 0); // Green
     delay(delayValue);
    b.allLedsOff();
    
    b.ledOn(9, 255, 0, 255); // Magenta
       delay(delayValue);
    b.allLedsOff();
}
    return 1;
}

void userEnterSequence(){
    if (b.buttonOn(1)) {
        b.ledOn(12, 255, 0, 0); // Red
         delay(400);
        b.allLedsOff();
        checkSequence = checkSequence + "A";
    }
    
    if (b.buttonOn(2)) {
        b.ledOn(3, 0, 255, 0); // Green
         delay(400);
        b.allLedsOff();
        checkSequence = checkSequence + "B";
    }
    
     if (b.buttonOn(3)) {
        b.ledOn(6, 0, 0, 255); // Blue
         delay(400);
        b.allLedsOff();
        checkSequence = checkSequence + "C";
     }
     
      if (b.buttonOn(4)) {
        b.ledOn(9, 255, 0, 255); // Magenta
         delay(400);
        b.allLedsOff();
        checkSequence = checkSequence + "D";
      }
  
}

void checkResult(){
    if (checkSequence == "ACBD"){  // this is the correct sequence
       b.allLedsOn(0,255,0); // if user entered correct sequence then blink green lights
       delay(400);
       b.allLedsOff();
       checkSequence = "";
      
    } 
   else {
      
       b.allLedsOff();
      
   }
  
}
// Restarting by phone app
int playAgain(String command){
    
    int valueReceived = atoi(command.c_str());
    if (valueReceived == 1){
    initSequence("500");
    userEnterSequence();
    checkResult();
     checkSequence = "";
    }
    return 1;
}
// Restarting by photon
void restartByPhoton(){
   
   if ((b.buttonOn(1) && b.buttonOn(2)) || (b.buttonOn(1) && b.buttonOn(3)) || (b.buttonOn(1) && b.buttonOn(4)) || (b.buttonOn(2) && b.buttonOn(1)) || (b.buttonOn(2) && b.buttonOn(3)) || (b.buttonOn(2) && b.buttonOn(4)) || (b.buttonOn(3) && b.buttonOn(1)) || (b.buttonOn(3) && b.buttonOn(4))){
    initSequence("500");
    userEnterSequence();
    checkResult();
     checkSequence = "";
    }
}




